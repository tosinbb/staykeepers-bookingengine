from rest_framework import generics
from .serializers import ListingSerializer
from .models import Listing
from rest_framework.views import APIView
from django.http import Http404
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q
import datetime

# Create your views here.


class ListingCreateView(generics.ListCreateAPIView):
    queryset = Listing.objects.all()
    serializer_class = ListingSerializer

    def perform_create(self, serializer):
        """Save the Listing data when creating a new Listing."""
        serializer.save()


class ListingDetailsView(APIView):
    """
    Retrieve a listing instance.
    """

    queryset = Listing.objects.all()
    serializer_class = ListingSerializer

    def get(self, request, format=None):
        max_price = request.GET.get("max_price", "")
        check_in = request.GET.get("check_in", "")
        check_out = request.GET.get("check_out", "")

        # Get Listings with price lower than max price
        available_listings = Listing.booking_info.filter(price__lt=max_price).order_by(
            "price"
        )

        # Convert input string time to python datetime format
        check_in = datetime.datetime.strptime(check_in, format="%Y/%m/%d").date()
        check_out = datetime.datetime.strptime(check_out, format="%Y/%m/%d").date()

        # Get Listings without blocked dates
        available_listings = available_listings.reservation.filter(
            ~Q(
                Q(check_in__gte=check_in) & Q(check_in__lte=check_out)
                | Q(check_out__gte=check_in) & Q(check_out__lte=check_out)
            )
        )

        serializer = ListingSerializer(available_listings)
        return Response(serializer.data)
