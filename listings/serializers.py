from rest_framework import serializers
from .models import Listing


class ListingSerializer(serializers.ModelSerializer):
    """Serializer to map the Listing Model instance into JSON format."""

    price = serializers.ReadOnlyField(source="self.booking_info.price", required=False)

    class Meta:
        """Meta class to map serializer's fields with the model fields."""

        model = Listing
        fields = ("listing_type", "title", "country", "city", "price")
