from django.test import TestCase
from .models import Listing, HotelRoomType, HotelRoom, BookingInfo, Reservation
from rest_framework.test import APIClient
from rest_framework.views import status
from django.urls import reverse


# Test Cases for the Listing Model
class ListingTestCase(TestCase):
    """This class defines the test suite for the Listing model."""

    def setUp(self):
        self.listing = Listing(
            listing_type="APARTMENT",
            title="Hotel Lux 5***",
            country="UK",
            city="London",
        )

    def test_create_listing(self):
        """Test the Listing model can create a listing."""
        old_count = Listing.objects.count()
        self.listing.save()
        new_count = Listing.objects.count()
        self.assertNotEqual(old_count, new_count)


# Test Cases for the Hotel Room Type Model and API
class HotelRoomTypeTestCase(TestCase):
    """This class defines the test suite for the Hotel Room Type model."""

    def setUp(self):
        self.listing = Listing(
            listing_type="APARTMENT",
            title="Hotel Lux 5***",
            country="UK",
            city="London",
        )
        self.hotel_room_type = HotelRoomType(hotel=self.listing, title="Hotel Lux 5***")

    def test_create_hotel_room_type(self):
        """Test the Hotel Room Type model can create a hotel room type."""
        old_count = HotelRoomType.objects.count()
        self.listing.save()
        self.hotel_room_type.save()
        new_count = HotelRoomType.objects.count()
        self.assertNotEqual(old_count, new_count)


# Test Cases for the Hotel Room Model and API
class HotelRoomTestCase(TestCase):
    """This class defines the test suite for the Hotel Room model."""

    def setUp(self):
        self.listing = Listing(
            listing_type="APARTMENT",
            title="Hotel Lux 5***",
            country="UK",
            city="London",
        )
        self.hotel_room_type = HotelRoomType(hotel=self.listing, title="Hotel Lux 5***")
        self.hotel_room = HotelRoom(
            hotel_room_type=self.hotel_room_type, room_number="11"
        )

    def test_create_hotel_room(self):
        """Test the Hotel Room model can create a hotel room."""
        old_count = HotelRoom.objects.count()
        self.listing.save()
        self.hotel_room_type.save()
        self.hotel_room.save()
        new_count = HotelRoom.objects.count()
        self.assertNotEqual(old_count, new_count)


# Test Cases for the Booking Info Model and API
class BookingInfoTestCase(TestCase):
    """This class defines the test suite for the Booking Info model."""

    def setUp(self):
        self.listing = Listing(
            listing_type="APARTMENT",
            title="Hotel Lux 5***",
            country="UK",
            city="London",
        )
        self.hotel_room_type = HotelRoomType(hotel=self.listing, title="Hotel Lux 5***")
        self.booking_info = BookingInfo(
            listing=self.listing, hotel_room_type=self.hotel_room_type, price=100.00
        )

    def test_create_booking_info(self):
        """Test the Booking Info model can create a booking info."""
        old_count = BookingInfo.objects.count()
        self.listing.save()
        self.hotel_room_type.save()
        self.booking_info.save()
        new_count = BookingInfo.objects.count()
        self.assertNotEqual(old_count, new_count)


# Test Cases for the Reservation Model and API
class ResevationTestCase(TestCase):
    """This class defines the test suite for the Reservation model."""

    def setUp(self):
        self.listing = Listing(
            listing_type="APARTMENT",
            title="Hotel Lux 5***",
            country="UK",
            city="London",
        )
        self.hotel_room_type = HotelRoomType(hotel=self.listing, title="Hotel Lux 5***")
        self.reservation = Reservation(
            listing=self.listing,
            hotel_room_type=self.hotel_room_type,
            check_in="2021-12-09",
            check_out="2021-12-12",
        )

    def test_create_reservation(self):
        """Test the Reservation model can create a reservation."""
        old_count = Reservation.objects.count()
        self.listing.save()
        self.hotel_room_type.save()
        self.reservation.save()
        new_count = Reservation.objects.count()
        self.assertNotEqual(old_count, new_count)


# Test Cases for the Listing API
class ListingViewTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""

        # Initialize client
        self.client = APIClient()
        self.data = {
            "listing_type": "apartment",
            "title": "Hotel Lux 5***",
            "country": "UK",
            "city": "London",
        }
        self.response = self.client.post(
            reverse("create_listing"), self.data, format="json"
        )

    def test_api_create_listing(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_api_get_listing(self):
        listings = Listing(
            listing_type="APARTMENT",
            title="Hotel Lux 5***",
            country="UK",
            city="London",
        )
        listings.save()
        response = self.client.get(
            "/api/v1/units/?max_price=100&check_in=2021-12-09&check_out=2021-12-12",
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, listings)