from django.urls import path, re_path, include
from .views import ListingDetailsView, ListingCreateView
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path("units/", ListingCreateView.as_view(), name="create_listing"),
    re_path(r"^units/$", ListingDetailsView.as_view(), name="listing_details"),
]

urlpatterns = format_suffix_patterns(urlpatterns)
